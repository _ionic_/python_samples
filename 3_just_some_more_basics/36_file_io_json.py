import json

with open('35_input.json', 'r') as f:
    entire_file_content = f.read()

    json_obj = json.loads(entire_file_content)

    json_obj["numbers"] = json_obj["numbers"][3:7]  # oh, slicing? cool!
    json_obj["string"] += 'bar'
    json_obj["boolean"] = not json_obj["boolean"]
    json_obj["new_key"] = 42

    with open('37_output.json', 'w') as g:
        new_content = json.dumps(json_obj, indent=4)

        g.write(new_content)
