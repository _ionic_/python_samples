f = open('32_input.csv', 'r')
for line in f.readlines():
    my_list = line.split(',')
    print(my_list)  # see the \n ? we don't want them; let's try again
f.close()

print()

f = open('32_input.csv', 'r')
for line in f.readlines():
    my_list = line.strip().split(',')  # strip is good; it helps us to chop the ends
    print(my_list)
f.close()

print()

# f = open; f.close; ugh.. we're lazy.. what if we forget to close the file? okay, let's try another approach..

with open('32_input.csv', 'r') as f:
    for line in f.readlines():
        my_list = line.strip().split(',')  # strip is good; it helps us with string ends
        # print(my_list)
        print('; '.join(my_list))  # join brings stuff together into a single string
        # notice there is '; ' only between the values
    # 'with' will close the file now

print()

with open('32_input.csv', 'r') as f:  # CommaSeparatedValues; 'r' is for read
    with open('34_output.usv', 'w') as g:  # UnderscoreSeparatedValues 'w' is for write
        for line in f.readlines():
            g.write('_'.join(line.strip().split(',')))
            g.write('\n')
