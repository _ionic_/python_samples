def function1(param1, param2, param3='ABC'):
    print(f"I am function1; called with {param1} {param2} {param3}", end=' -> ')
    return param1 + param2


print(function1(1, 2))
print(function1(1, 2, 3))

print(function1('1', '2'))
print(function1('1', '2', '3'))

print(function1([1], [2]))
print(function1([1], [2], [3]))

# where is function2 ?! oh.. right.. I forgot to call it..
