import random as rnd

my_list = []  # 'list' is a reserved word
for _ in range(10):  # '_' is often used as 'not-used-variable'
    my_list.append(rnd.randint(1, 10))
print(my_list)

# see the code above? ugh.. too much lines, too much characters.. we are lazy..

my_other_list = [rnd.randint(1, 10) for _ in range(10)]  # yeah, this is better even for my eyes :)
print(my_other_list)

# remember dicts iterations? let's revert a dict!

my_dict = {
    "A": "X",
    "B": "Y",
    "C": "Z",
}
print(my_dict)

my_out_dict = {}
for key, value in my_dict.items():
    my_out_dict[value] = key
print(my_out_dict)

# or..

my_other_dict = [(value, key) for key, value in my_dict.items()]
print(my_other_dict)

# but wait.. this is not a dictionary; it is a list of pairs (a.k.a. 2-tuples); let's fix it by only hocus-pocus-ing it to a dict! *magic*

my_other_actual_dict = dict([(value, key) for key, value in my_dict.items()])
print(my_other_actual_dict)

# 'Y' key bothers you? kick it out!

del my_other_actual_dict['Y']
print(my_other_actual_dict)
print('Y: but.. but.. BUT Y?! Y ME?!?')
