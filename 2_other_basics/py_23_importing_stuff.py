import py_21_module_with_sum as xyz  # alias is optional, for convenience

from py_22_module_with_mul_div import my_mul  # import only one item, not all of them

# notice the printings and compare them with those in the previous 2 scripts

print(xyz.my_add(1, 2))
print(xyz.my_sub(2, 1))
print(my_mul(2, 3))
print(my_div(3, 2))  # yep, this is not available because of the import

# notice: sometimes the errors (in console, when you run the script) may by non-synced with normal output (short-reason: stdout / stderr)
