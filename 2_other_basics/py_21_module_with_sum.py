def my_add(a, b):
    return a + b


def my_sub(a, b):
    return a - b


# don't panic! there are just a few 'special keywords' like "__keyword__"


if __name__ == '__main__':
    print(f'[ inside if] print from py_21_...; __name__ = {__name__}; __file__ = {__file__}')

print(f'[outside if] print from py_21_...; __name__ = {__name__}; __file__ = {__file__}')
