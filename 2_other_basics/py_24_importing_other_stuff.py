
# --------------------------- first lib
print('--------------------------- random')
import random as rnd

print('0 <=', rnd.random(), '< 1')

my_list = []  # 'list' is a reserved word
for x in range(10):  # notice that x is not used
    my_list.append(rnd.randint(1, 10))
print(my_list)
print()


# --------------------------- second lib
print('--------------------------- json')
import json  # notice: no alias

my_str = ''' { "python_is_cool" : true , "python_has" : { "just_a_few_libs" : false, "a_lot_of_power" : "YES!" } , 
    "version we like": "Python 3+ (latest if possible)" , "version we try to avoid": "Python 2"} '''
my_json_object = json.loads(my_str)
print(my_json_object)  # this is parsed as a python object
print(json.dumps(my_json_object))  # this is the correct form of JSON standard
print(json.dumps(my_json_object, sort_keys=True, indent=4))  # it also has some cool parameters
print()


# --------------------------- third lib
print('--------------------------- requests')
import requests

# ImportError? try from cmd: pip install requests
# more import/pip errors? please don't skip them... we need them

response = requests.get('https://api.coindesk.com/v1/bpi/currentprice.json')
print(response.status_code)
print('Plain text:', response.text)

json_object = json.loads(response.text)
print('JSON Object:', json.dumps(json_object, indent=4))
print(f"1 BTC = {json_object['bpi']['USD']['rate_float']} USD")  # this may fail if coindesk changes it's json's structure over time
print()
