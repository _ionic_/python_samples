def my_mul(a, b):
    return a * b


def my_div(a, b):
    return a / b


if __name__ == '__main__':
    print(f'[ inside if] print from py_22_...; __name__ = {__name__}; __file__ = {__file__}')

print(f'[outside if] print from py_22_...; __name__ = {__name__}; __file__ = {__file__}')
