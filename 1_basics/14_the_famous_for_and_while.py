# keywords: while; continue; break;

nr = 0
while nr < 10000:
    nr = nr + 1
    if nr % 1003 == 0:
        print(nr, nr / 1_000, nr // 1_000)
        # notice the difference between / and //
        # notice the _ written inside the number (it's just for readability)

nr = 0
while True:
    nr += 1
    if nr % 2 == 0:
        continue
    if nr > 10:
        break
    print(nr)

# for! what is this for?! it's for a lot of things...

for i in range(10):  # notice the limits: [0, 10) with step 1
    print(i, end=' ')  # trick to use space instead of newline
print()

for i in range(5, 15):  # notice the limits: [5, 15) with step 1
    print(i, end=' ')
print()

for i in range(20, 30, 2):  # notice the limits: [20, 30) with step 2
    print(i, end=' ')
print()

# easy, right? let's see some more stuff:

for i in ['eggs', 'bread', 'milk']:
    print(f'please buy {i}', end='; ')
print()

# notice the following 2 uses of 'in':

for i in "the fox jumped over the lazy dog":
    print(f'[{i}]', end='')
print()

sentence = "the fox jumped over the lazy dog"
if 'a' in sentence:
    print('a exists in the sentence')

# for ... in ... -> iterates through
# if ... in ... -> checks if found

# awesome!
