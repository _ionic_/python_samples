my_dict = {
    "a": "x",
    "b": "y",
    "c": "z",
}

for key in my_dict.keys():
    print('key', key)

for value in my_dict.values():
    print('value', value)

for key, value in my_dict.items():
    print('pair', key, value)
