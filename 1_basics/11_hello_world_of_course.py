# Comment on single line


'''Comment
    on
    multiple
    lines
'''

"""Comment
    on
    multiple
    lines
"""

# Run current file in PyCharm shortcut: CTRL+SHIFT+F10


print('Hello World')

print("Hello World")

print('''Hello World''')

print("""Hello World""")

print()  # prints only the newline

print('''Hello
    World
    On
    Multiple
    Lines''')

print("""Hello
    World
    On
    Multiple
    Lines""")

print()

your_name = input('Please let me know your name: ')

print('Hello', your_name)
print('Please use CTRL+SHIFT+F10 on each of the following Python scripts.')
print('Enjoy!')
