
print("Hello", "Johnny")  # notice the space added by 'print' function

print("Hello" + "Johnny")  # notice the concatenation

print("Hello, {name}".format(name='Johnny'))

my_username = 'Johnny'
my_password = "123456"

print("Hello! I'm {username}, and my password is: {password}".format(username=my_username, password=my_password))

print(f"Hello! I'm {my_username}, and my password is: {my_password}")  # easier way

# notice each newline added by 'print'

print       (                       "Let me tell you that..",
"in math," ,                    1       ,
        'plus'          ,
                                    int('1')   , "=" ,
        'exactly two')

# bad syntax formatting? try: CTRL+ALT+L


# let's see some variable types:

print("string", 'this is a string')

print("string", "this is another string")

print("integer", 123456)

print("float", 123456.789)

print("list", [1, 2, 3, 4])

print("list", ['a', 'b', 'c'])

print("list", ['X', 4, "Y", 2])

print("dictionary", {"a": 1, "b": 2})

print("dictionary", {"key": "value", "grocery_list": ['eggs', 'bread', 'milk']})

print("boolean", True)  # capitalized

print("boolean", False)  # capitalized

# aaand the staaar of the day:

print("the NoneType:", None)  # it's like.. nothing.. but it's still something :)
