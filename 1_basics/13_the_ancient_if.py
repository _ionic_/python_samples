# simple if/else

you_like_this = True

if you_like_this:
    print('yey')  # notice the indentation: it's python's power to understand that
else:
    exit(-13)  # this will stop EVERYTHING! even the time and space; and the universe will implode!

# extended if/elif/else

birth_year = 2022
if 2021 < birth_year:
    print('Yes, I know, you are Chuck Norris...')
elif 1996 <= birth_year and birth_year <= 2021:
    print("Let's say that you are from Gen Z")  # notice the use of single/double quotes
elif 1980 <= birth_year <= 1995:  # notice the use of double comparison in the same expression
    print("Look, we have someone from Generation Y!")
else:
    print("Older than 40. Congrats for your age!")

'''
interesting keywords to know: 
    - if
    - elif
    - else
    - and
    - or
    - not
    - is
    - is not
    - in
    - not in
'''

for a in [True, False, None]:
    for b in [True, False, None]:
        # print(a, 'is not', b, '=', a is not b)  # too much commas and quotes
        print(f'{a} is not {b} = {a is not b}')
